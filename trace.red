Red [
	title: "TRACE & TRACE-DEEP mezzanines"
	author: @hiiamboris
	license: 'BSD-3
]


;@@ BUG: this diverts return & exit (but bind-only is too slow to use here); also probably break/continue-sensitive
trace: func [
	"Evaluate each expression in CODE and pass it's result to the INSPECT function"
	inspect	[function!] "func [result [any-type!] next-code [block!]]"
	code	[block!]	"If empty, still evaluated once"
	/local r
][
	; #assert [2 = preprocessor/func-arity? spec-of :inspect]
	; #assert [[any-type!] = first find spec-of :inspect block!]
	until [
		set/any 'r do/next code 'code						;-- eval at least once - to pass unset from an empty block
		inspect :r code
		tail? code
	]
	:r
]


;; this is handy for producing a step-by-step expression reduction log
;; [100 / add 1 subtract 4 2] should produce:
;;  subtract 4 2
;;  add 1 :.
;;  100 / :.
;; then `trace` the result and log the outputs
;; @@ TODO: it's simple now - no lit/get-args support; handle with care or extend
trace-deep: function [
	"Deeply trace a set of expressions"			;@@ TODO: remove `quote` once apply is available
	inspect	[function!] "func [expr [block!] result [any-type!]]"
	code	[block!]	"If empty, still evaluated once"
][
	code: copy/deep code		;-- will be modified in place
	eval-types: reduce [		;-- value types that should be traced
		paren!		;-- recurse into it
		; block!	-- never known if it's data or code argument - can't recurse into it
		set-word!	;-- ignore it, as it's previous value is not used; also preprocessor returns it as a single expression
		set-path!	;-- ditto
		word!		;-- function call or value acquisition - we wanna know the value
		path!		;-- ditto
		get-word!	;-- value acquisition - wanna know it
		get-path!	;-- ditto
		native!		;-- literal functions should be evaluated but no need to display their source
		action!
		routine!
		op!
		function!
	]

	wrap: func [x [any-type!]] [
		either any [			;-- quote evaluatable types
			any-word? :x
			any-path? :x
			any-function? :x
			paren? :x
		][
			as paren! reduce ['quote :x]
		][
			:x
		]
	]
	rewrite: func [code] [
		while [not empty? code] [
			code: rewrite-next code
		]
	]
	rewrite-single: func [code /local expr] [
		expr: copy/part code next code
		change/only code wrap inspect expr do expr
	]
	rewrite-next: func [code /no-op /local start end v1 v2 r arity expr rewrite? _] [
		; assert [not empty? code]
		;; correct for the set-word / set-path bug - rewrite every set-expr with it's result
		start: code
		while [find [set-word! set-path!] type?/word :start/1] [start: next start]
		end: preprocessor/fetch-next start

		;; 2 or more values
		v1: :start/1
		v2: :start/2
		rewrite?: yes
		case [									;-- priority: op, any-func, everything else
			all [									;-- operator - recurse into it's right part
				word? :v2
				op! = type? get/any :v2
			][
				if no-op [rewrite?: no]
				if find eval-types type? :v1 [rewrite-single start]
				rewrite-next/no-op skip start 2
			]

			all [									;-- a function call - recurse into it
				any [
					word? :v1
					all [path? :v1  set [v1 _] preprocessor/value-path? v1]
				]
				find [native! action! function! routine!] type?/word get/any v1
			][
				arity: either path? v1: get v1 [
					preprocessor/func-arity?/with spec-of :v1 start/1
				][
					preprocessor/func-arity? spec-of :v1
				]
				end: next start
				loop arity [end: rewrite-next end]
			]

			paren? :v1 [rewrite as block! v1]		;-- recurse into paren

			'else [									;-- other cases
				if find eval-types type? :v1 [rewrite-single start]
				rewrite?: any [
					not same? end next start		;-- 2 or more values
					not same? start code			;-- got set-words/set-paths
				]
			]
		]
		if rewrite? [
			expr: copy/deep/part code end			;-- have to make a copy or it may be modified by `do`
			set/any 'r inspect expr do/next code 'end
			change/part/only code wrap :r end
		]
		return next code
	]
	rewrite code
	do code
]

; inspect: func [e [block!] r [any-type!]] [print [pad mold :r 10 "<=" mold/flat/only e] :r]
; x: y: 2 f: func [x] [x * 5]
; probe trace-deep :inspect [x * x + f y]

inspect:      func [r [any-type!] more [block!]] [print [pad mold/part/flat :r 20 20 " next: " mold/part/flat/only more 40]]
inspect-deep: func [e [block!] r [any-type!]]    [print [pad mold/part/flat/only e 20 20 " => " mold/part/flat :r 40] :r]

show-trace: function [code [block!]] [trace-deep :inspect-deep code]

; #assert [() = trace-deep :inspect-deep []]
; #assert [() = trace-deep :inspect-deep [()]]
; #assert [() = trace-deep :inspect-deep [1 ()]]
; #assert [3  = trace-deep :inspect-deep [1 + 2]]
; #assert [9  = trace-deep :inspect-deep [1 + 2 * 3]]
; #assert [4  = trace-deep :inspect-deep [x: y: 2 x + y]]
; #assert [20 = trace-deep :inspect-deep [f: func [x] [does [10]] g: f 1 g * 2]]
; #assert [20 = trace-deep :inspect-deep [f: func [x] [does [10]] (g: f (1)) ((g) * 2)]]

; #assert [() = trace-deep func [x y [any-type!]][:y] []]
; #assert [() = trace-deep func [x y [any-type!]][:y] [()]]
; #assert [() = trace-deep func [x y [any-type!]][:y] [1 ()]]
; #assert [3  = trace-deep func [x y [any-type!]][:y] [1 + 2]]
; #assert [9  = trace-deep func [x y [any-type!]][:y] [1 + 2 * 3]]
; #assert [4  = trace-deep func [x y [any-type!]][:y] [x: y: 2 x + y]]
; #assert [20 = trace-deep func [x y [any-type!]][:y] [f: func [x] [does [10]] g: f 1 g * 2]]
; #assert [20 = trace-deep func [x y [any-type!]][:y] [f: func [x] [does [10]] x: f: :f (g: f (1)) ((g) * 2)]]

comment {
	Example:
	>> show-trace [x: y: 2 x + y]
	x: y: 2               =>  2
	x                     =>  2
	y                     =>  2
	2 + 2                 =>  4
	== 4
}