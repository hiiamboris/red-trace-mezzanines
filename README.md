# OBSOLETE
### See https://gitlab.com/hiiamboris/red-mezz-warehouse#general-purpose instead


# Red Trace mezzanines

Highly experimental!

```
	>> show-trace [x: y: 2 x + y]
	x: y: 2               =>  2
	x                     =>  2
	y                     =>  2
	2 + 2                 =>  4
	== 4
```

- `trace-deep` and `show-trace` allow you to review the evaluation log step by step
- `trace` is a base for much more advanced tools (will add later), but is handy to discover where an error occurred in the code
